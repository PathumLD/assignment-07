//Write a program to find the Frequency of a given character in a given string.

#include<stdio.h>
int main(){
   int x=0;
   char ch, str[100] = {'\0'};
   printf("Input a String : ");
   gets(str);
   printf("Input a Character : ");
   scanf("%c", &ch);
   for(int i=0; str[i] != '\0'; i++){
      if(ch == str[i]){
         x++;}
      }
   printf("Frequency of '%c' is %d", ch, x);
   return 0;
}
