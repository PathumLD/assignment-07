//Write a C program to reverse a sentence entered by user


#include <stdio.h>
int main(){

   char rev[50] = {'\0'};

   printf("Input a Sentence : ");

   gets(rev);

   int len = sizeof(rev);

   printf("Reversed Sentence :\n ");

   for(int i=len; i>0; i--){
      printf("%c", rev[i-1]);
      }

   return 0;
}
